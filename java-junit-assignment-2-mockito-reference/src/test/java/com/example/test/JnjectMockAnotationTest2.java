/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.Application2;
import com.example.Calculation2;
import com.example.NumberUtils;

/**
 * Dec 26, 2018
 */
@ExtendWith(MockitoExtension.class)
public class JnjectMockAnotationTest2 {
  @Mock
  private Calculation2 calculation;
  @Spy
  private NumberUtils numberUtils;
  @InjectMocks
  private Application2 application;

  @Test
  public void testCheckSub_thenReturnFalse() {
    Mockito.when(calculation.sub(2, 10)).thenReturn(-8);
    Mockito.when(numberUtils.sub(2, 10)).thenReturn(9);
    assertEquals(Boolean.FALSE, application.checkSub(2, 10));
  }
}
