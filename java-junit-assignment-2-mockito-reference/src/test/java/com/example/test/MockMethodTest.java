/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Jan 3, 2019
 */
@SuppressWarnings("unchecked")
public class MockMethodTest {
  
  @Test
  public void testMock_sizeMethod() {
    List<String> mockList = mock(List.class);
    when(mockList.size()).thenReturn(5);
    
    assertTrue(5 == mockList.size());
  }
  
  @Test
  public void testMock_getMethod() {
    List<String> mockList = mock(List.class);
    when(mockList.get(5)).thenReturn("five");
    
    assertEquals("five", mockList.get(5));
  }
  
}
