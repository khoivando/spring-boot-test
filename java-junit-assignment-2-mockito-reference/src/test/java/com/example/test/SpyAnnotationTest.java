/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

/**
 * Jan 3, 2019
 */
public class SpyAnnotationTest {
  @Spy
  Utils mockUtils;
  @BeforeEach
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }
  @Test
  public void testMock_realMethod() {
    // mocked method
    when(mockUtils.add(1, 1)).thenReturn(5);
    assertEquals(5, mockUtils.add(1, 1));

    // real method called since it's not stubbed
    assertEquals(20, mockUtils.add(19, 1));
  }
}

class Utils {
  public int add(int x, int y) {
    System.out.println("params: "+x+","+y);
    return x+y;
  }
}
