/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.Application;
import com.example.Calculation;

/**
 * Dec 26, 2018
 */
@ExtendWith(MockitoExtension.class)
public class ApplicationMock2Test {

  @Mock
  private Calculation calculation;

  @Test
  public void testCheckSub_thenReturnFalse() {
    Application application = new Application(calculation);
    Mockito.when(calculation.sub(2, 10)).thenReturn(-8);
    assertEquals(Boolean.FALSE, application.checkSub(2, 10));
  }

}
