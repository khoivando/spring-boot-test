/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.example.Application;
import com.example.Calculation;

/**
 * Dec 26, 2018
 */
public class ApplicationMock1Test {

  @Test
  public void testCheckSum_thenReturnTrue() {
    Calculation mockCalculation = Mockito.mock(Calculation.class);
    Application application = new Application(mockCalculation);
    
    Mockito.when(mockCalculation.sum(2, 10)).thenReturn(12);
    assertEquals(Boolean.TRUE, application.checkSum(2, 10));
  }
}
