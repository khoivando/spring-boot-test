/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.ArgumentMatchers.isNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import com.example.AbstractList;
/**
 * Jan 4, 2019
 */
@ExtendWith(MockitoExtension.class)
public class VerifyingVoidMethodTest {

  @Test
  public void whenAddCalledVerified() {
    MyList mockVoid = mock(MyList.class);
    mockVoid.add(0, "");

    verify(mockVoid, times(1)).add(0, "");;
  }

  @Test
  public void whenAddCalledValueCaptured() {
    MyList mockVoid = mock(MyList.class);
    ArgumentCaptor<String> valueCapture = ArgumentCaptor.forClass(String.class);
    doNothing().when(mockVoid).add(any(Integer.class), valueCapture.capture());
    mockVoid.add(0, "captured");
    assertEquals("captured", valueCapture.getValue());
  }

  @org.junit.Test(expected = Exception.class)
  public void givenNull_AddThrows() {
    MyList myList = mock(MyList.class);
    doThrow().when(myList).add(isA(Integer.class), isNull());
    myList.add(0, null);
  }

  @Test
  public void whenAddCalledAnswered() {
    MyList mockVoid = mock(MyList.class);
    doAnswer((Answer<Void>) invocation -> {
      Object arg0 = invocation.getArgument(0);
      Object arg1 = invocation.getArgument(1);

      //do something with the arguments here
      assertEquals(3, arg0);
      assertEquals("answer me", arg1);

      return null;
    }).when(mockVoid).add(any(Integer.class), any(String.class));
    mockVoid.add(3, "answer me");
  }

  @Test
  public void whenAddCalledRealMethodCalled() {
    MyList mockVoid = mock(MyList.class);
    doCallRealMethod().when(mockVoid).add(any(Integer.class), any(String.class));
    mockVoid.add(1, "real");
    verify(mockVoid, times(1)).add(1, "real");
  }

}

class MyList extends AbstractList<String> {
  @Override
  public void add(int index, String element) {
    System.out.println("called");
  }
}