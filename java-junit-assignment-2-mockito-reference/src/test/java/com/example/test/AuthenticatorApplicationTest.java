/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.AuthenticatorApplication;
import com.example.AuthenticatorInterface;
import com.example.EmptyCredentialsException;

/**
 * Dec 27, 2018
 */
public class AuthenticatorApplicationTest {

  @Mock
  AuthenticatorInterface authenticatorMock;
  @InjectMocks
  AuthenticatorApplication application;

  @BeforeEach
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testAuthenticateTrue() {
    String username = "username";
    String password = "123456";

    when(authenticatorMock.authenticateUser(username, password)).thenReturn(Boolean.TRUE);

    boolean actual = application.authenticate(username, password);

    // These will make the test fail!
    // verify(authenticatorMock).authenticateUser(username, "not the original password");
    // verify(authenticatorMock, never()).authenticateUser(username, password);

    verify(authenticatorMock, times(1)).authenticateUser(username, password);
    verify(authenticatorMock, atLeastOnce()).authenticateUser(username, password);
    verify(authenticatorMock, atLeast(1)).authenticateUser(username, password);
    verify(authenticatorMock, atMost(1)).authenticateUser(username, password);

    assertTrue(actual);
  }

  @Test
  public void testAuthenticateFalse() {
    String username = "username";
    String password = "1234567";
    when(authenticatorMock.authenticateUser(username, password)).thenReturn(Boolean.FALSE);

    boolean actual = application.authenticate(username, password);
    verify(authenticatorMock, times(1)).authenticateUser(username, password);

    assertFalse(actual);
  }

  @Test
  public void testAuthenticateEmpty() throws EmptyCredentialsException {
    String username = "";
    String password = "123";
    when(authenticatorMock.authenticateUser(username, password)).thenThrow(new EmptyCredentialsException());
    
    assertThrows(EmptyCredentialsException.class, () -> {
      application.authenticate(username, password);
    });
  }

}
