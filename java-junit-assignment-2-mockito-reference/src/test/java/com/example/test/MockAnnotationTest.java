/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * Jan 3, 2019
 */
public class MockAnnotationTest {

  @Mock
  List<String> mockList;

  @BeforeEach
  public void setup() {
    // void NullPointerException
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void test() {
    when(mockList.get(0)).thenReturn("JournalDev");
    assertEquals("JournalDev", mockList.get(0));
  }

}
