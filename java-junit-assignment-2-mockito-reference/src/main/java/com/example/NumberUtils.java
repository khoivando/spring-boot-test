/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example;

import org.springframework.stereotype.Component;

/**
 * Jan 4, 2019
 */
@Component
public class NumberUtils {
  
  public int sum(int number1, int number2) {
    return number1 + number2;
  }
  
  public int sub(int number1, int number2) {
    return number1 - number2;
  }
  
}
