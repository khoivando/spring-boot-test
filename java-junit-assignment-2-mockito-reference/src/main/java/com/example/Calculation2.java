/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Dec 26, 2018
 */
@Component
public class Calculation2 {
  
  @Autowired NumberUtils numberUtils;
  
  public int sum(int number1, int number2) {
    return numberUtils.sum(number1, number2);
  }
  
  public int sub(int number1, int number2) {
    return numberUtils.sub(number1, number2);
  }
  
}
