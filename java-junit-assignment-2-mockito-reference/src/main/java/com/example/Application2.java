/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Dec 26, 2018
 */
@Service
public class Application2 {

  @Autowired
  private Calculation2 calculation;
  
  @Autowired 
  private NumberUtils numberUtils;

  public boolean checkSum(int number1, int number2) {
    int sum = calculation.sum(number1, number2);
    int sum2 = numberUtils.sum(number1, number2);
    
    return sum == sum2;
  }

  public boolean checkSub(int number1, int number2) {
    int sub = calculation.sub(number1, number2);
    int sub2 = numberUtils.sub(number1, number2);
    
    System.out.println("sub: "+ sub);
    System.out.println("sub2: "+ sub2);
    
    return sub == sub2;
  }
   
}
