/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example;

/**
 * Dec 27, 2018
 */
@SuppressWarnings("serial")
public class EmptyCredentialsException extends RuntimeException {

  public EmptyCredentialsException() {
    super("Empty credentials!");
  }

}
