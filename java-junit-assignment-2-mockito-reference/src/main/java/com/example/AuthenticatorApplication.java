/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example;

/**
 * Dec 27, 2018
 */
public class AuthenticatorApplication {

  private AuthenticatorInterface authenticator;

  public AuthenticatorApplication(AuthenticatorInterface authenticator) {
    this.authenticator = authenticator;
  }

  /**
   * Tries to authenticate an user with the received user name and password, with the received
   * AuthenticatorInterface interface implementation in the constructor.
   *
   * @param username The user name to authenticate.
   * @param password The password to authenticate the user.
   * @return True if the user has been authenticated; false if it has not.
   */
  public boolean authenticate(String username, String password) {
    return authenticator.authenticateUser(username, password);
  }
  
}
