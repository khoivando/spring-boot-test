/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example;

/**
 * Dec 27, 2018
 */
public class AuthenticatorImpl implements AuthenticatorInterface {

  public boolean authenticateUser(String username, String password) throws EmptyCredentialsException {
    if(username.isEmpty() || password.isEmpty()) throw new EmptyCredentialsException();
    if("username".equals(username) && "123456".equals(password)) return true;
    return false;
  }

}
