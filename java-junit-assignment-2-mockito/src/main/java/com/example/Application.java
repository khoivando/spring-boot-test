/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Dec 26, 2018
 */
@Service
public class Application {

  @Autowired
  private Calculation calculation;

  public Application(Calculation calculation) {
    super();
    this.calculation = calculation;
  }

  public Calculation getCalculation() {
    return calculation;
  }

  public void setCalculation(Calculation calculation) {
    this.calculation = calculation;
  }

  public boolean checkSum(int number1, int number2) {
    int sum = calculation.sum(number1, number2);
    return sum > 10;
  }

  public boolean checkSub(int number1, int number2) {
    int sub = calculation.sub(number1, number2);
    return sub > 0;
  }

}
