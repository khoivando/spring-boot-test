/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example;

import org.springframework.stereotype.Component;

/**
 * Dec 26, 2018
 */
@Component
public class Calculation {
  
  public int sum(int number1, int number2) {
    return number1 + number2;
  }
  
  public int sub(int number1, int number2) {
    return number1 - number2;
  }
  
}
