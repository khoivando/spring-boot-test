/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.Application;
import com.example.Calculation;

/**
 * Dec 27, 2018
 */
@ExtendWith(MockitoExtension.class)
public class ApplicationMock3Test {

  // Complete this test based on ApplicationTest.class
  // - using @Mock, Mockito.mock, Mockito.when for Calculation.class
  // - using @InjectMocks for Application
  @Test
  public void testCheckSub_thenReturnFalse() {
    fail("please impl to pass");
  }

}
