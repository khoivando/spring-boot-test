/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.example.Application;
import com.example.Calculation;

/**
 * Dec 27, 2018
 */
public class ApplicationTest {

  @Test
  public void testCheckSum() {
    Application application = new Application(new Calculation());
    boolean c = application.checkSum(2, 12);
    assertEquals(Boolean.TRUE, c);
  }
  
  @Test
  public void testCheckSub() {
    Application application = new Application(new Calculation());
    boolean c = application.checkSub(2, 12);
    assertEquals(Boolean.FALSE, c);
  }

}
