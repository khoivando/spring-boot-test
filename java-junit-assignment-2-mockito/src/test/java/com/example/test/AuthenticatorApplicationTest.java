/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.AuthenticatorApplication;
import com.example.AuthenticatorInterface;
import com.example.EmptyCredentialsException;

/**
 * Dec 27, 2018
 */
public class AuthenticatorApplicationTest {

  @Mock
  AuthenticatorInterface authenticatorMock;
  
  @InjectMocks
  AuthenticatorApplication application;

  @BeforeEach
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testAuthenticateTrue() {
    String username = "username";
    String password = "123456";
    when(authenticatorMock.authenticateUser(username, password)).thenReturn(Boolean.TRUE);

    boolean actual = application.authenticate(username, password);
    // These will make the test fail, please update to PASS
    verify(authenticatorMock).authenticateUser(username, "not the original password");
    assertTrue(actual);
  }

  @Test
  public void testAuthenticateFalse() {
    fail("please impl to pass");
  }

  @Test
  public void testAuthenticateEmpty() throws EmptyCredentialsException {
    String username = "";
    String password = "123";
    when(authenticatorMock.authenticateUser(username, password)).thenThrow(new EmptyCredentialsException());
    // test fail, please add more code to PASS
    // ...
  }

}
