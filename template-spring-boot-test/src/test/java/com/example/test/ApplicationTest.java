package com.example.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.app.TemplateSpringBootTestApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TemplateSpringBootTestApplication.class)
public class ApplicationTest {

  @Test
  public void contextLoads() throws Exception {
  }

}

