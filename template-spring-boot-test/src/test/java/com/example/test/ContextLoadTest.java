package com.example.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.app.HomeController;
import com.example.app.TemplateSpringBootTestApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TemplateSpringBootTestApplication.class)
public class ContextLoadTest {

  @Autowired
  private HomeController controller;
  
	@Test
	public void contextLoads() {
	  assertThat(controller).isNotNull();
	}

}

