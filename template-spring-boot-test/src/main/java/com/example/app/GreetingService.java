/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app;

import org.springframework.stereotype.Service;

/**
 * Jan 5, 2019
 */
@Service
public class GreetingService {

  public String greet() {
    return "Hello World";
  }
  
}
