/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Jan 5, 2019
 */
@RestController
public class GreetingController {
  
  @Autowired
  GreetingService service;
  
  @RequestMapping("greeting")
  public String greeting() {
    return service.greet();
  }
  
}
