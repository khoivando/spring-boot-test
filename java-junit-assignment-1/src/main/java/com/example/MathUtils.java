/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example;

/**
 * Dec 26, 2018
 */
public class MathUtils {

  public static int div(int number1, int number2) {
    if (number2 == 0) {
      throw new ArithmeticException("divide by zero");
    }
    return number1/number2;
  }

}
