/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 * Dec 26, 2018
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AssertTest {

  //  Please using assertions corresponding for each test below
  
  @Test
  public void testAssertArrayEquals() {
    byte[] expected = "trial".getBytes();
    byte[] actual = "trial".getBytes();
    fail("please impl to pass");
  }

  @Test
  public void testAssertEquals() {
    fail("please impl to pass");
  }

  @Test
  public void testAssertNotEquals() {
    fail("please impl to pass");
  }

  @Test
  public void testAssertFalse() {
    fail("please impl to pass");
  }
  
  @Test
  public void testAssertTrue() {
    fail("please impl to pass");
  }

  @Test
  public void testAssertNotNull() {
    fail("please impl to pass");
  }

  @Test
  public void testAssertNotSame() {
    fail("please impl to pass");
  }

  @Test
  public void testAssertNull() {
    fail("please impl to pass");
  }

  @Test
  public void testAssertSame() {
    Integer aNumber = Integer.valueOf(123768);
    fail("please impl to pass");
  }

}
