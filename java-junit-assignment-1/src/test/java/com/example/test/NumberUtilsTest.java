/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.example.NumberUtils;

/**
 * Dec 26, 2018
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NumberUtilsTest {

  //  Please using assertions to complete for each test below, if false please update code to pass

  @Test
  public void test_1IsNotPrimeNumber() {
    int number = -1;
    fail("please impl to pass");
  }

  @Test
  public void test0IsNotPrimeNumber() {
    int number = 0;
    fail("please impl to pass");
  }

  @Test
  public void test1IsNotPrimeNumber() {
    int number = 1;
    fail("please impl to pass");
  }

  @Test
  public void test4IsNotPrimeNumber() {
    int number = 4;
    fail("please impl to pass");
  }

  @Test
  public void test2IsPrimeNumber() {
    int number = 2;
    fail("please impl to pass");
  }

  @Test
  public void test5IsPrimeNumber() {
    int number = 5;
    fail("please impl to pass");
  }

  @Test
  public void testMaxOfArray() {
    int [] array = { 1, 2, 3, 4, 100, 5, 6, 7, 8, 9 };
    int max = NumberUtils.max(array);
    fail("please impl to pass");
  }

  @Test
  public void testMinOfArray() {
    int [] array = { -1, 2, 3, 4, -500, 6, 7, 8, -9 };
    int min = NumberUtils.min(array);
    fail("please impl to pass");
  }
}
