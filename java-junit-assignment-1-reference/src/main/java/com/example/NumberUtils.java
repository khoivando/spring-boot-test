/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example;

import java.util.Arrays;

/**
 * Dec 26, 2018
 */
public class NumberUtils {

  public static boolean isPrimeNumber(int number) {
    
    for (int i = 2; i < number; i++) {
      if (number % i == 0) return false;
    }
    return true;
  }

  public static int max(int ... array) {
    return Arrays.stream(array).max().getAsInt();
  }

  public static int min(int ... array) {
    return Arrays.stream(array).min().getAsInt();
  }

}
