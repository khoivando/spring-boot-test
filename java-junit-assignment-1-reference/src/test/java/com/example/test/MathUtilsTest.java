/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.example.MathUtils;

/**
 * Dec 26, 2018
 */
public class MathUtilsTest {

  @Test(expected = ArithmeticException.class) 
  public void testDiv_whitExpected() throws Exception {
    MathUtils.div(1, 0);
  }

  @Test
  public void testDiv_withCatchException() {
    try {
      MathUtils.div(1, 0);
    } catch (Exception e) {
      assertThat(e, instanceOf(ArithmeticException.class));
      assertEquals(e.getMessage(), "divide by zero");
    }
  }

  @Test
  public void testDiv_withFail() throws Exception {
    try {
      MathUtils.div(1, 1);
    } catch (Exception e) {
      fail("Not throw exception");
    }
  }

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void testDiv_withRule() throws Exception {
    thrown.expect(ArithmeticException.class);
    thrown.expectMessage("divide by zero");
    MathUtils.div(1, 0);
  }

}
