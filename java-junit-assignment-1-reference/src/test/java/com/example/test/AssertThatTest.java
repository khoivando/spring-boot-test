/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HandshakeCompletedEvent;

import org.hamcrest.Matchers;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 * Dec 26, 2018
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AssertThatTest {

  // Please using assertions corresponding for each test below

  @Test
  public void testAssertThatBothContainsString() {
    String string = "khoidv";
    assertThat(string, both(containsString("k")).and(containsString("v")));
  }

  @Test
  public void testAssertThatEveryItemContainsString() {
    List<String> strings = Arrays.asList("funny", "banner", "internet");
    assertThat(strings, everyItem(containsString("n")));
  }

  @Test
  public void testAssertThatMatchers_AllOf() {
    String string = "good catch";
    assertThat(string, allOf(equalTo("good catch"), startsWith("good"), endsWith("catch")));
  }

  @Test
  public void testAssertThatMatchers_NotAllOf() {
    String string = "bad";
    assertThat(string, not(allOf(equalTo("bad"), equalTo("good"))));
  }

  @Test
  public void testAssertThatMatchers_AnyOf() {
    String string = "good catch";
    assertThat(string, anyOf(equalTo("bad"), equalTo("good catch")));
  }

  @Test
  public void testAssertThatMatchers_NotSameInstance() {
    Object objectA = new Object();
    Object objectB = new Object();
    assertThat(objectA, not(sameInstance(objectB)));
  }

  // Please write test method for each org.hamcrest.Matchers function test below
  
  //  Core
      //  anything
      //  describedAs
      //  is

  //  Logical
      //  allOf
      //  anyOf
      //  not
  
  //  Object
      //  equalTo
      //  hasToString
      //  instanceOf, isCompatibleType
      //  notNullValue, nullValue
      //  sameInstance
  
  //  Beans
      //  hasProperty
  
  //  Collections
      //  array
      //  hasEntry, hasKey, hasValue
      //  hasItem, hasItems
      //  hasItemInArray
  
  //  Number
      //  closeTo
      //  greaterThan, greaterThanOrEqualTo, lessThan, lessThanOrEqualTo
  
  //  Text
      //  equalToIgnoringCase
      //  equalToIgnoringWhiteSpace
      //  containsString, endsWith, startsWith

}
