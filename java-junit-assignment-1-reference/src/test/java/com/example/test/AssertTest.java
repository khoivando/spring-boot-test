/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 * Dec 26, 2018
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AssertTest {

  //  Please using assertions corresponding for each test below
  
  @Test
  public void testAssertArrayEquals() {
    byte[] expected = "trial".getBytes();
    byte[] actual = "trial".getBytes();
    assertArrayEquals("arrays should be equal", expected, actual);
  }

  @Test
  public void testAssertEquals() {
    assertEquals("values should be equal", "text", "text");
  }

  @Test
  public void testAssertNotEquals() {
    assertNotEquals("values should be different", "text", "text1");
  }

  @Test
  public void testAssertFalse() {
    assertFalse("valude should be false", false);
  }
  
  @Test
  public void testAssertTrue() {
    assertTrue("should be true", true);
  }

  @Test
  public void testAssertNotNull() {
    assertNotNull("valude should not be null", new Object());
  }

  @Test
  public void testAssertNotSame() {
    assertNotSame("should not be same Object", new Object(), new Object());
  }

  @Test
  public void testAssertNull() {
    assertNull("should be null", null);
  }

  @Test
  public void testAssertSame() {
    Integer aNumber = Integer.valueOf(123768);
    assertSame("should be same", aNumber, aNumber);
  }

}
