/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Dec 26, 2018
 */
@RunWith(Suite.class)
@SuiteClasses({
   AssertTest.class, 
   AssertThatTest.class, 
   NumberUtilsTest.class
})
// Please config to run tests:  AssertTest,  AssertThatTest, NumberUtilsTest
public class AssertTestSuite {

}
