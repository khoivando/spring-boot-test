/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.AfterClass;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.Employee;
import com.example.repository.EmployeeRepository;
import com.example.service.EmployeeService;
import com.example.service.impl.EmployeeServiceImpl;

/**
 * Jan 4, 2019
 */

@ExtendWith(SpringExtension.class)
public class EmployeeServiceTest {

  @TestConfiguration
  static class EmployeeServiceImplTestContextConfiguration {
    @Bean
    public EmployeeService employeeService() {
      return new EmployeeServiceImpl();
    }
  }

  @Autowired
  private EmployeeService employeeService;

  @MockBean
  private EmployeeRepository employeeRepository;

  @Test
  public void whenValidFirstName_thenEmployeeShouldBeFound() {
    Optional<Employee> optional = Optional.ofNullable(new Employee("do", "khoi", 32, "phone"));

    String firstName = "do";
    when(employeeRepository.findByFirstName(firstName)).thenReturn(optional);

    Employee found = employeeService.findByFirstName(firstName);
    assertThat(firstName).isEqualTo(found.getFirstName());
  }

  @AfterClass
  public static void tearDown() {
  }

}
