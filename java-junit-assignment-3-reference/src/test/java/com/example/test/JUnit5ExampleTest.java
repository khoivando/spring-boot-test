/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import org.junit.jupiter.api.Test;

/**
 * Jan 4, 2019
 */
public class JUnit5ExampleTest {
  @Test
  void justAnExample() {
      System.out.println("This test method should be run");
  }
}
