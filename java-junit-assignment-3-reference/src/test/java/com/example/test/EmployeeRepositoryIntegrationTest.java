/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.Employee;
import com.example.repository.EmployeeRepository;

/**
 * Jan 3, 2019
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
// @DataJpaTest provides some standard setup needed for testing the persistence layer:
//  configuring H2, an in-memory database
//  setting Hibernate, Spring Data, and the DataSource
//  performing an @EntityScan
//  turning on SQL logging
public class EmployeeRepositoryIntegrationTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private EmployeeRepository employeeRepository;

  @Test
  public void whenFindByFistName_thenReturnEmployee() {
    Employee employee = new Employee("do", "khoi", 32, "0976896988");
    entityManager.persist(employee);
    entityManager.flush();
    System.out.println("===========>>>>>>>>>>>>>>> employeeId: "+ employee.getId());

    Optional<Employee> optional = employeeRepository.findByFirstName(employee.getFirstName());
    assertThat(optional.get().getFirstName()).isEqualTo(employee.getFirstName());
  }

  @Test
  public void whenFindById_thenReturnEmployee() {
    Employee employee = new Employee("van", "khoi", 32, "0976896988");
    entityManager.persistAndFlush(employee);
    System.out.println("===========>>>>>>>>>>>>>>> employeeId: "+ employee.getId());

    Employee fromDb = employeeRepository.findById(employee.getId()).orElse(null);
    assertThat(employee.getFirstName()).isEqualTo(fromDb.getFirstName());
  }

  @Test
  public void whenInvalidName_thenReturnNull() {
    Optional<Employee> optional = employeeRepository.findByFirstName("doesNotExist");
    assertThat(optional.isPresent()).isFalse();
  }

  @Test
  public void whenInvalidId_thenReturnNull() {
    Employee employee = employeeRepository.findById(-11l).orElse(null);
    assertThat(employee).isNull();
  }

  @Test
  public void givenSetOfEmployees_whenFindAll_thenReturnAllEmployees() {
    Employee alex = new Employee("alxex", "khoi", 32, "0976896988");
    Employee ron = new Employee("ron", "khoi", 32, "0976896988");
    Employee bob = new Employee("bob", "khoi", 32, "0976896988");

    entityManager.persist(alex);
    entityManager.persist(bob);
    entityManager.persist(ron);
    entityManager.flush();

    List<Employee> allEmployees = employeeRepository.findAll();
    assertThat(allEmployees).hasSize(3).extracting(Employee::getFirstName).containsOnly(alex.getFirstName(), ron.getFirstName(), bob.getFirstName());
  }

}
