/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.example.controller.EmployeeRestController;
import com.example.model.Employee;
import com.example.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Jan 4, 2019
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(EmployeeRestController.class)
public class EmployeeControllerIntegrationTest {

  @Autowired
  private MockMvc mvc;

  @MockBean
  private EmployeeService service;

  @Test
  public void whenPostEmployee_thenCreateEmployee() throws Exception {
    Employee employee = new Employee("firstName", "lastName", 32, "phone");
    given(service.add(Mockito.any())).willReturn(employee);

    mvc.perform(post("/employee/add")
        .contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper()
        .writeValueAsString(employee)))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.firstName", is("firstName")));

    verify(service, VerificationModeFactory.times(1)).add(Mockito.any());
    reset(service);
  }

  @Test
  public void givenEmployees_whenGetEmployees_thenReturnJsonArray() throws Exception {
    Employee employee = new Employee("firstName", "lastName", 32, "phone");
    List<Employee> allEmployees = Arrays.asList(employee);

    //given(service.findAll()).willReturn(allEmployees);
    when(service.findAll()).thenReturn(allEmployees);

    mvc.perform(get("/employee/all").contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$", hasSize(1)))
    .andExpect(jsonPath("$[0].firstName", is(employee.getFirstName())));
  }
}
