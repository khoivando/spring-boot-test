/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.Employee;
import com.example.repository.EmployeeRepository;

/**
 * Jan 4, 2019
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class EmployeeRepositoryTest {
  
  @Autowired
  private EmployeeRepository employeeRepository;

  @Test
  public void whenFindByFistName_thenReturnEmployee() {
    Employee employee = new Employee("do", "khoi", 32, "0976896988");
    employeeRepository.save(employee);
    
    System.out.println("===========>>>>>>>>>>>>>>> employeeId: "+ employee.getId());

    Optional<Employee> optional = employeeRepository.findByFirstName(employee.getFirstName());
    assertThat(optional.get().getFirstName()).isEqualTo(employee.getFirstName());
  }
  
}
