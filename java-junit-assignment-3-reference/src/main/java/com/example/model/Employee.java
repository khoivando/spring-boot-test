/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Jan 3, 2019
 */
@Entity
@Table(name = "employee")
public class Employee {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String firstName;
  private String lastName;
  private int age;
  private String phone;
  
  public Employee() {
    super();
  }

  public Employee(String firstName, String lastName, int age, String phone) {
    super();
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.phone = phone;
  }
  
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  public int getAge() {
    return age;
  }
  public void setAge(int age) {
    this.age = age;
  }
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public String toString() {
    return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", age="+ age + ", phone=" + phone + "]";
  }
  
}
