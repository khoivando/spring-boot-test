/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Jan 3, 2019
 */
public class AbstractServiceImpl<T, ID extends Serializable, R extends JpaRepository<T, ID>> {

  @Autowired
  protected R repo;

  public T add(T t) {
    return repo.save(t);
  }

  public T update(T t) {
    return repo.save(t);
  }

  public Optional<T> get(ID id) {
    return repo.findById(id);
  }

  public void delete(ID id) {
    repo.deleteById(id);
  }

  public List<T> findAll() {
    return repo.findAll(new Sort(Direction.DESC, "id"));
  }

  public List<T> findAllAndAscSort() {
    return repo.findAll(new Sort(Direction.ASC, "id"));
  }

  public Page<T> findAll(Pageable pageable) {
    return repo.findAll(pageable);
  }

  public boolean exists(ID id) {
    return repo.existsById(id);
  }

}
