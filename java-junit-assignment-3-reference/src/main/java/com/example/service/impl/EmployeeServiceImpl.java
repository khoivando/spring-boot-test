/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.service.impl;

import org.springframework.stereotype.Service;

import com.example.model.Employee;
import com.example.repository.EmployeeRepository;
import com.example.service.EmployeeService;

/**
 * Jan 3, 2019
 */
@Service
public class EmployeeServiceImpl extends AbstractServiceImpl<Employee, Long, EmployeeRepository> implements EmployeeService {

  @Override
  public Employee findByFirstName(String firstName) {
    return repo.findByFirstName(firstName).get();
  }
  
}
