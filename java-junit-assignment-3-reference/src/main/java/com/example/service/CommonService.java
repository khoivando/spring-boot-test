/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Jan 3, 2019
 */
public interface CommonService<T, ID extends Serializable> {

  T add(T entity);

  T update(T entity);

  Optional<T> get(ID id);

  void delete(ID id);

  List<T> findAll();

  Page<T> findAll(Pageable pageable);

  boolean exists(ID id);

}
