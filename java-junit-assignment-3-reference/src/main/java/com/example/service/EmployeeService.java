/***************************************************************************
 * Copyright 2018 - All rights reserved.                *    
 **************************************************************************/
package com.example.service;

import com.example.model.Employee;

/**
 * Jan 3, 2019
 */
public interface EmployeeService extends CommonService<Employee, Long> {
  
  Employee findByFirstName(String firstName);
  
}
