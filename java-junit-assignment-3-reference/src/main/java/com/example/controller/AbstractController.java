/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Jan 4, 2019
 */
public class AbstractController<S> {

  @Autowired
  protected S service;
  
}
