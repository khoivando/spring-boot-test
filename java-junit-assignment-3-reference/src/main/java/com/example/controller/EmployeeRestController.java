/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Employee;
import com.example.service.EmployeeService;

/**
 * Jan 4, 2019
 */
@RestController
@RequestMapping("employee")
public class EmployeeRestController extends AbstractController<EmployeeService> {

  @PostMapping("add")
  public ResponseEntity<Employee> add(@RequestBody Employee employee) {
    Employee saved = service.add(employee);
    return new ResponseEntity<>(saved, HttpStatus.CREATED);
  }

  @GetMapping("all")
  public List<Employee> findAll() {
    return service.findAll();
  }

}
