package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaJunitAssignment3ReferenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaJunitAssignment3ReferenceApplication.class, args);
	}

}

